import 'package:flutter/material.dart';
import 'package:mercado_libre_scraping/utils/service.dart';

class AddLaptopForm extends StatefulWidget {

  const AddLaptopForm({super.key});

  @override
  State<AddLaptopForm> createState() => _AddLaptopFormState();
}

class _AddLaptopFormState extends State<AddLaptopForm> {
  final _nameController = TextEditingController();
  final _priceController = TextEditingController();
  final _urlController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _priceController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: const Text("Add Laptop"),
        backgroundColor: Colors.cyan,
      ),
      body: Padding(
        padding: const EdgeInsets.all(40),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Text(
              'Name',
              textAlign: TextAlign.left,
              style: TextStyle(color: Color.fromARGB(255, 105, 105, 105))
            ),
            SizedBox(
              height: 20,
              child: TextField(
                controller: _nameController,
              ),
            ),
            const SizedBox(
              height: 22,
            ),
            const Text(
              'Price',
              textAlign: TextAlign.left,
              style: TextStyle(color: Color.fromARGB(255, 105, 105, 105))
            ),
            SizedBox(
              height: 20,
              child: TextField(
                controller: _priceController,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 17),
              child: const Text(
                'Url',
                textAlign: TextAlign.left,
                style: TextStyle(color: Color.fromARGB(255, 105, 105, 105))
              ),
            ),
            SizedBox(
              height: 50,
              child: TextField(
                controller: _urlController,
              ),
            ),
            SizedBox(
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: () {
                      
                    },
                    child: const Text('Cancel'),
                  ),
                  TextButton(
                    onPressed: () {
                      void _postData(name,price,url) async {
                        await ApiService().addLaptop(name,price,url);
                        Future.delayed(const Duration(seconds: 1)).then((value) => setState(() {}));
                      }
    
                      _postData(_nameController.text, _priceController.text, _urlController.text);
    
                    },
                    child: const Text('Confirm'),
                  ),
                ],
              )
            ),
          ]
        ),
      ),
    );
  }
}
