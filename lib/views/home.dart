import 'package:flutter/material.dart';
import 'package:mercado_libre_scraping/utils/laptops.dart';
import '../utils/service.dart';
import 'addForm.dart';

class CardCustom extends StatelessWidget{
  List<Laptops> carsData= [];
  int index;
  CardCustom(this.carsData, this.index, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 3,
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
        child: SizedBox(
          width: 370,
          height: 400,
          child: ListTile(
          contentPadding: const EdgeInsets.fromLTRB(15, 10, 25, 0),
          title: Column(
            children: [
              Image(image: Image.network(carsData[index].url!).image,),
              Text("${carsData[index].name}"),
            ],
          ),
        ),
        ),
      ),
    );
  } 

}


class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late List<Laptops>? laptopModel = [];


  void _getData() async {
    laptopModel = (await ApiService().getLaptops())!;
    Future.delayed(const Duration(seconds: 1)).then((value) => setState(() {}));
    //print(laptopModel);
  }

  @override

  Widget build(BuildContext context) {
     _getData();
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddLaptopForm()),
              ); 
          },
          child: const Icon(Icons.add),
      ),
        appBar: AppBar(
        title: const Text("Laptops"),
        backgroundColor: Colors.cyan,
        ),
        body: ListView.builder(
          itemCount: laptopModel!.length,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (_, index) {
            return ListTile(
              title: GestureDetector(
                  onTap: () {
 
                  },
                  child: Stack(
                    children: [
                     CardCustom(laptopModel!,index),
                    ])
                  ));
          }
        )); 
  }
}