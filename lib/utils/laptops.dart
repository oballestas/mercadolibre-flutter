class Laptops {
  final id;
  final name;
  final price;
  final url;

  Laptops({
    required this.id,
    required this.name,
    required this.price,
    required this.url,
  });

  factory Laptops.fromJson(Map<String, dynamic> json) => Laptops(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "url": url,
      };
}



