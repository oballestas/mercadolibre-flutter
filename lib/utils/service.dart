import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:mercado_libre_scraping/utils/api_constants.dart';
import 'laptops.dart';


class ApiService {
  Future<List<Laptops>?> getLaptops() async {
    try {
      var url = Uri.parse(ApiDrfConstants.baseUrl + ApiDrfConstants.endpoint);
      var response = await http.get(url);
      if (response.statusCode == 200) {
        
        List<dynamic> results = jsonDecode(response.body)["results"];
        return results.map((result) => Laptops.fromJson(result)).toList();

      }
    } catch (e) {
      log(e.toString());
    }
  }

    Future<List<Laptops>?> addLaptop(name,price,urlImage) async {
    try {
      var url = Uri.parse(ApiDrfConstants.baseUrl + ApiDrfConstants.endpoint);
      var response = await http.post(url,
                                    body:jsonEncode({
                                        'name': name.toString(),
                                        'price': price.toString(),
                                        'url': Uri.parse(urlImage).toString(),
                                    }),
                                    headers: {
                                      "content-type": "application/json"
                                    }
                                );
      if (response.statusCode == 201) {
        getLaptops();
      }
    } catch (e) {
      log(e.toString());
    }
  }
}

